package com.jackson.calandario;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private TextView titulo;


    Calendar cal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cal = GregorianCalendar.getInstance();

        titulo =  findViewById(R.id.mesAno);

        atualizarTitulo(cal.getTime());


    }


    public void proximoMes(View view){
        cal.add(Calendar.MONTH, +1);
        atualizarTitulo(cal.getTime());


    }

    public void mesAnterior(View view){
        cal.add(Calendar.MONTH, -1);
        atualizarTitulo(cal.getTime());
    }

    public void dataAtual(View view){
        cal = GregorianCalendar.getInstance();
        atualizarTitulo(cal.getTime());
    }

    public void atualizarTitulo(Date data){
        Locale local = new Locale("pt", "BR");
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM/yyyy", local);
        this.titulo.setText(dateFormat.format(data));
    }


}
